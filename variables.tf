variable "branch_name" {
  default     = "master"
  description = "branch used from codecommit."
  type        = string
}
variable "buildspec" {
  description = "batch script for build phase."
  type        = string
}
variable "build_compute_type" {
  default     = "BUILD_GENERAL1_SMALL"
  description = "needed when small compute type is not enough to compile"
  type        = string
}
variable "codepipeline_bucket" {
  description = "bucketname used from pipeline to pass configurations needed for codebuild."
  type        = string
}
variable "deploy_environment" {
  description = "test or prod environment."
  type        = string
}
variable "deploy_template_name" {
  default     = "exists"
  description = "test or prod environment."
  type        = string
}
variable "deployspec" {
  description = "batch script for deploy phase."
  type        = string
}
variable "env_in_repository_name" {
  description = "name of the repository with 'env' after prefix used if same account is shared"
  type        = string
}

variable "environment_variables" {
  default     = []
  description = "no more used - deprecated."
  type        = list(any)
}
variable "force_approve" {
  type        = string
  default     = "false"
  description = "if false than an approve is requested, otherwise there is no approve phase after build and before deploy"
}
variable "image" {
  default     = "aws/codebuild/docker:18.09.0"
  description = "image that codebuild uses to build"
  type        = string
}
variable "kms_arn" {
  description = "kms keys used to crypt bucket to enable cross account access for prod -> test"
  type        = string
}
variable "poll_for_source_changes" {
  default     = "true"
  description = "using events for trigger build."
  type        = bool
}
variable "repository_name" {
  description = "name of the repository inferred by directory name."
  type        = string
}
variable "role_arn_codebuild" {
  description = "role used for codebuild phase."
  type        = string
}
variable "role_arn_codepipeline" {
  description = "role used by codepipeline."
  type        = string
}
variable "role_arn_source" {
  description = "role used to take from alternative AWS account."
  type        = string
}
variable "s3_cache" {
  description = "s3 bucket cache name."
  type        = string
}
variable "shared_account" {
  description = "to indicate that in same environment is present more than on deploy so different naming convention must be applied for ECR repos and pipelines"
  type        = bool
  default     = false
}
variable "tags" {
  default     = {}
  description = "s3 bucket cache name."
  type        = map(any)
}
