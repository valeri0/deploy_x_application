## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_codebuild_project.build_app](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codebuild_project) | resource |
| [aws_codebuild_project.deploy_service](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codebuild_project) | resource |
| [aws_codepipeline.codepipeline](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/codepipeline) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_branch_name"></a> [branch\_name](#input\_branch\_name) | branch used from codecommit. | `string` | `"master"` | no |
| <a name="input_build_compute_type"></a> [build\_compute\_type](#input\_build\_compute\_type) | needed when small compute type is not enough to compile | `string` | `"BUILD_GENERAL1_SMALL"` | no |
| <a name="input_buildspec"></a> [buildspec](#input\_buildspec) | batch script for build phase. | `any` | n/a | yes |
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | cluster name | `string` | n/a | yes |
| <a name="input_codepipeline_bucket"></a> [codepipeline\_bucket](#input\_codepipeline\_bucket) | bucketname used from pipeline to pass configurations needed for codebuild. | `any` | n/a | yes |
| <a name="input_deploy_environment"></a> [deploy\_environment](#input\_deploy\_environment) | test or prod environment. | `any` | n/a | yes |
| <a name="input_deployspec"></a> [deployspec](#input\_deployspec) | batch script for deploy phase. | `any` | n/a | yes |
| <a name="input_environment_variables"></a> [environment\_variables](#input\_environment\_variables) | no more used - deprecated. | `list(any)` | `[]` | no |
| <a name="input_force_approve"></a> [force\_approve](#input\_force\_approve) | if false than an approve is requested, otherwise there is no approve phase after build and before deploy | `string` | `"false"` | no |
| <a name="input_image"></a> [image](#input\_image) | image that codebuild uses to build | `string` | `"aws/codebuild/docker:18.09.0"` | no |
| <a name="input_kms_arn"></a> [kms\_arn](#input\_kms\_arn) | kms keys used to crypt bucket to enable cross account access for prod -> test | `string` | n/a | yes |
| <a name="input_poll_for_source_changes"></a> [poll\_for\_source\_changes](#input\_poll\_for\_source\_changes) | using events for trigger build. | `string` | `"true"` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | prefix name for infrastructure, ex. fdh, dpl, bitots | `string` | n/a | yes |
| <a name="input_repository_name"></a> [repository\_name](#input\_repository\_name) | name of the repository inferred by directory name. | `any` | n/a | yes |
| <a name="input_role_arn"></a> [role\_arn](#input\_role\_arn) | assumed to create infrastructure in enviroment where .hcl is ran. | `any` | n/a | yes |
| <a name="input_role_arn_codebuild"></a> [role\_arn\_codebuild](#input\_role\_arn\_codebuild) | role used for codebuild phase. | `any` | n/a | yes |
| <a name="input_role_arn_codepipeline"></a> [role\_arn\_codepipeline](#input\_role\_arn\_codepipeline) | role used by codepipeline. | `any` | n/a | yes |
| <a name="input_role_arn_source"></a> [role\_arn\_source](#input\_role\_arn\_source) | role used to take from alternative AWS account. | `any` | n/a | yes |
| <a name="input_s3_cache"></a> [s3\_cache](#input\_s3\_cache) | s3 bucket cache name. | `any` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | s3 bucket cache name. | `map` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cloudwatch_build_log"></a> [cloudwatch\_build\_log](#output\_cloudwatch\_build\_log) | logs for building phase |
| <a name="output_cloudwatch_deploy_log"></a> [cloudwatch\_deploy\_log](#output\_cloudwatch\_deploy\_log) | logs for deploy phase |
| <a name="output_codepipeline_service_arn"></a> [codepipeline\_service\_arn](#output\_codepipeline\_service\_arn) | arn of service codepipeline |
