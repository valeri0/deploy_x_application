resource "aws_codebuild_project" "build_app" {
  build_timeout = "300"
  name          = "${var.env_in_repository_name}-build-app"
  service_role  = var.role_arn_codebuild
  tags          = var.tags
  cache {
    type     = "S3"
    location = var.s3_cache
  }
  artifacts {
    type = "CODEPIPELINE"
  }
  source {
    type      = "CODEPIPELINE"
    buildspec = var.buildspec
  }
  environment {
    compute_type    = var.build_compute_type
    image           = var.image
    type            = "LINUX_CONTAINER"
    privileged_mode = true
    dynamic "environment_variable" {
      for_each = [for v in var.environment_variables : {
        name  = v.name
        value = v.value
      }]

      content {
        name  = environment_variable.value.name
        value = environment_variable.value.value
      }
    }
  }
}

resource "aws_codebuild_project" "deploy_service" {
  for_each      = var.deploy_template_name != null ? toset(["0"]) : toset([])
  build_timeout = "300"
  name          = "${var.env_in_repository_name}-deploy-app"
  service_role  = var.role_arn_codebuild
  tags          = var.tags
  cache {
    type  = "LOCAL"
    modes = ["LOCAL_DOCKER_LAYER_CACHE", "LOCAL_CUSTOM_CACHE"]
  }
  artifacts {
    type = "CODEPIPELINE"
  }
  source {
    type      = "CODEPIPELINE"
    buildspec = var.deployspec
  }
  environment {
    compute_type    = "BUILD_GENERAL1_SMALL"
    image           = var.image
    type            = "LINUX_CONTAINER"
    privileged_mode = true
    dynamic "environment_variable" {
      for_each = [for v in var.environment_variables : {
        name  = v.name
        value = v.value
      }]

      content {
        name  = environment_variable.value.name
        value = environment_variable.value.value
      }
    }
  }
}

resource "aws_codepipeline" "codepipeline" {
  name     = var.shared_account ? var.env_in_repository_name : var.repository_name
  role_arn = var.role_arn_codepipeline
  artifact_store {
    location = var.codepipeline_bucket
    type     = "S3"
    dynamic "encryption_key" {
      for_each = var.deploy_environment != "prod" ? [1] : []
      content {
        id   = var.kms_arn
        type = "KMS"
      }
    }
  }
  stage {
    name = "Source"
    action {
      name             = "Source"
      category         = "Source"
      owner            = "AWS"
      provider         = "CodeCommit"
      role_arn         = var.deploy_environment != "prod" ? var.role_arn_source : null
      version          = "1"
      output_artifacts = ["source_output"]
      configuration = {
        PollForSourceChanges = var.poll_for_source_changes
        RepositoryName       = var.repository_name
        BranchName           = var.branch_name
      }
    }
  }
  stage {
    name = "build_app"
    action {
      name             = "Build"
      category         = "Build"
      owner            = "AWS"
      provider         = "CodeBuild"
      version          = "1"
      input_artifacts  = ["source_output"]
      output_artifacts = ["build_output"]
      configuration = {
        ProjectName = aws_codebuild_project.build_app.name
      }
    }
  }
  dynamic "stage" {
    for_each = var.force_approve == "false" ? [1] : []
    content {
      name = "Approve"

      action {
        name     = "Approval"
        category = "Approval"
        owner    = "AWS"
        provider = "Manual"
        version  = "1"

        configuration = {
          CustomData = "There is need for manual approval"
        }
      }
    }
  }
  dynamic "stage" {
    for_each = var.deploy_template_name != null ? [1] : []
    content {
      name = "deploy_app"
      action {
        name            = "Build"
        category        = "Build"
        owner           = "AWS"
        provider        = "CodeBuild"
        version         = "1"
        input_artifacts = ["build_output"]
        configuration = {
          ProjectName = aws_codebuild_project.deploy_service["0"].name
        }
      }
    }
  }
  tags = var.tags
}
