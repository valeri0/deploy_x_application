output "cloudwatch_build_log" {
  description = "logs for building phase"
  value       = "/aws/codebuild/${aws_codebuild_project.build_app.name}"
}
output "cloudwatch_deploy_log" {
  description = "logs for deploy phase"
  # value       = var.deploy_template_name
  value = var.deploy_template_name != null ? "/aws/codebuild/${aws_codebuild_project.deploy_service["0"].name}" : null
}
output "codepipeline_service_arn" {
  description = "arn of service codepipeline"
  value       = aws_codepipeline.codepipeline
}

